$(document).ready(function() {
    onLoad()
})

// var host = "http://127.0.0.1:9898"
var host = "http://128.199.254.13:9898"

document.getElementById('filter').addEventListener('click', handleFilter)
document.getElementById('search').addEventListener('click', curateData)
document.getElementById('seller_list_btn').addEventListener('click', showSellerList)

document.body.addEventListener('click', function(evt) {
    if (evt.target.className === 'btn btn-sm btn-block btn-primary') {
        curateDataV2(evt.target.id)
    }
}, false)

var publicUsername = ""

function onLoad() {

    swal({
        title: "Please Wait",
        text: "Preparing workspace ...",
        icon: "info",
        closeOnClickOutside: false,
        button: false,
    })

    setVisible("search_bar", 0)
    setVisible("header", 0)
    setVisible("history", 0)
    getSellerList()

    // curateDataV2("ankerindonesia")
    // setVisible("header", 1)
    // setVisible("history", 1)
    // setVisible("seller_list", 0)

}

// source from table
function curateDataV2(username) {

    let d = new Date(),
        diff = 30
    let finish = d.toISOString().slice(0, 10)
    d.setDate(d.getDate() - diff)
    let start = d.toISOString().slice(0, 10)

    document.getElementById("start").value = start
    document.getElementById("finish").value = finish
    document.getElementById("period").innerHTML = start + " - " + finish

    applyDayDiff(diff)
    getInfo(username)
    getHistory(username, start, finish)
    getStatistic(username, start, finish)

    publicUsername = username
}

// back to seller list component
function showSellerList() {
    setVisible("header", 0)
    setVisible("history", 0)
    setVisible("seller_list", 1)
}
// source from search bar currently is hidden
function curateData() {
    let username = document.getElementById('username').value

    let d = new Date()
    let finish = d.toISOString().slice(0, 10)
    d.setDate(d.getDate() - 30)
    let start = d.toISOString().slice(0, 10)

    getHistory(username, start, finish)
}

//getting seller list
function getSellerList() {
    swal({
        title: "Please Wait",
        text: "Getting available seller...",
        icon: "info",
        closeOnClickOutside: false,
        button: false,
    })
    let t0 = performance.now()
    fetch(host + "/api/v2/shp/seller-list")
        .then((res => {
            return res.json()
        }))
        .then((data => {

            if (data.data == null) {
                throw new Error(data.message)
            }
            let t1 = performance.now()
            swal({
                title: "Success",
                text: "Get Seller in " + parseInt(t1 - t0) + "ms",
                icon: "success",
                button: false,
            })

            let output = ""
            data.data.forEach(hist => {
                output += handleRowSeller(hist)
            })
            document.getElementById('seller_table').innerHTML = output

        })).catch(error => {
            swal("Not Found", error.message, "error")
        })
}

//getting info
function getInfo(username) {

    swal({
        title: "Please Wait",
        text: "Getting seller info data...",
        icon: "info",
        closeOnClickOutside: false,
        button: false,
    })

    let t0 = performance.now()
    fetch(host + "/api/shp/seller-info?username=" + username)
        .then((res => {
            return res.json()
        }))
        .then((data => {

            if (data.data == null) {
                throw new Error(data.message)
            }

            handleInfo(data.data)
            setVisible("header", 1)
            setVisible("seller_list", 0)

        })).catch(error => {
            swal("Not Found", error.message, "error")
        })


}

//getting statistic
function getStatistic(username, start, finish) {

    fetch(host + "/api/shp/seller-revenue?date_start=" + start + "&date_finish=" + finish + "&username=" + username)
        .then((res => {
            return res.json()
        }))
        .then((data => {

            if (data.data == null) {
                throw new Error(data.message)
            }



            handleStatistic(data.data)

        })).catch(error => {
            swal("Not Found", error.message, "error")
        })

}
//getting history
function getHistory(username, start, finish) {

    swal({
        title: "Please Wait",
        text: "Getting seller transaction data...",
        icon: "info",
        closeOnClickOutside: false,
        button: false,
    })

    let t0 = performance.now()
    fetch(host + "/api/shp/seller-history?date_start=" + start + "&date_finish=" + finish +
            "&username=" + username + "&history_type=ACTIVE_PRODUCT")
        .then((res => {
            return res.json()
        }))
        .then((data => {

            if (data.data == null) {
                throw new Error(data.message)
            }
            let t1 = performance.now()

            let output = "",
                prevHist = {},
                lastActiveProduct = 0

            let diff = {
                active_item: 0,
                sold_count: 0,
                review_count: 0,
                view_count: 0
            }

            data.data.forEach(hist => {

                diff.active_item = hist.active_item - prevHist.active_item
                diff.sold_count = hist.sold_count - prevHist.sold_count
                diff.review_count = hist.review_count - prevHist.review_count
                diff.view_count = hist.view_count - prevHist.view_count

                output += handleRowHist(hist, diff)
                prevHist = hist

                lastActiveProduct = hist.active_item
            })

            document.getElementById('history_table').innerHTML = output
            document.getElementById('active_product').innerHTML = lastActiveProduct

            let t2 = performance.now()

            swal({
                title: "Success",
                text: data.message,
                icon: "success",
                button: false,
            })

            setVisible("header", 1)
            setVisible("history", 1)
            setVisible("seller_list", 0)

        })).catch(error => {
            swal("Not Found", error.message, "error")
        })


}

// -- supporting func --

function handleInfo(data) {
    document.getElementById('name').innerHTML = data.name
    document.getElementById('followers').innerHTML = formatCurrency(data.follower_count, 0)
    document.getElementById('join_date').innerHTML = formatDate(data.update_time)
    document.getElementById('display_picture').src = data.portrait
    formatShopType(data.is_official_shop)
}

function handleStatistic(data) {
    document.getElementById('total_revenue').innerHTML = formatCurrency(data.total_revenue, 1)
    document.getElementById('total_sold').innerHTML = formatCurrency(data.total_sold, 0)
    document.getElementById('total_review').innerHTML = formatCurrency(data.total_review, 0)
    document.getElementById('total_view').innerHTML = formatCurrency(data.total_view, 0)
}

function handleFilter() {
    let start = document.getElementById('start').value
    let finish = document.getElementById('finish').value
    document.getElementById("period").innerHTML = start + " - " + finish

    applyDayDiff(formatDayDiff(start, finish))

    let username = publicUsername
    getHistory(username, start, finish)
    getStatistic(username, start, finish)
}

function handleRowHist(hist, diff) {
    return `
        <tr>
            <td><b>${formatDate(hist.update_time)}</b></td>
            <td>${hist.active_item}${formatDiff(diff.active_item)}</td>
            <td>${hist.sold_count}${formatDiff(diff.sold_count)}</td>
            <td>${hist.review_count}${formatDiff(diff.review_count)}</td>
            <td>${hist.view_count}${formatDiff(diff.view_count)}</td>
            <td>${hist.rating_avg}</td>
            <td>Rp ${hist.price_min} - Rp ${hist.price_max}</td>
        </tr>
    `
}

function handleRowSeller(data) {
    return `
        <tr>
            <td>${data.shopid}</td>
            <td>${data.name}</td>
            <td>${data.username}</td>
            <td><button id="${data.username}" class="btn btn-sm btn-block btn-primary">view</button></td>
        </tr>
    `
}


// -- util func --

function applyDayDiff(diff) {
    var elms = document.getElementsByClassName('day_diff')
    for (var i = 0; i < elms.length; i++) {
        elms[i].innerHTML = diff
    }
}

function formatDayDiff(start, finish) {
    ds = new Date(start)
    df = new Date(finish)
    return parseInt((df.getTime() - ds.getTime()) / (24 * 3600 * 1000))
}

function formatCurrency(num, fixed) {
    if (num === null) { return null } // terminate early
    if (num === 0) { return '0' } // terminate early
    fixed = (!fixed || fixed < 0) ? 0 : fixed // number of decimal places to show
    var b = (num).toPrecision(2).split("e"), // get power
        k = b.length === 1 ? 0 : Math.floor(Math.min(b[1].slice(1), 14) / 3), // floor at decimals, ceiling at trillions
        c = k < 1 ? num.toFixed(0 + fixed) : (num / Math.pow(10, k * 3)).toFixed(1 + fixed), // divide by power
        d = c < 0 ? c : Math.abs(c), // enforce -0 is 0
        e = d + ['', 'K', 'M', 'B', 'T'][k] // append power
    return e
}

function formatShopType(type) {
    let label = "Merchant"
    if (type == true) {
        label = "Official Store"
        document.getElementById("shop_type_icon").classList.add('fa', 'fa-check-circle')
        document.getElementById("shop_type_icon").style.color = "#1d89cc"
    } else {
        document.getElementById("shop_type_icon").classList.add('fa', 'fa-star')
        document.getElementById("shop_type_icon").style.color = "#f7ca00"
    }
    document.getElementById('shop_type_label').innerHTML = label
}

function formatDiff(num) {

    let strnum = num.toString(),
        color = "badge-secondary"

    if (isNaN(num)) {
        strnum = 0
    } else if (num > 0) {
        strnum = "+" + strnum
        color = "badge-success"
    } else if (num < 0) {
        color = "badge-danger"
    }

    return `<span class="badge ${color} pull-right">${strnum}</span>`
}

function formatDate(input) {
    return new Date(input).toISOString().slice(0, 10)
}

function setVisible(id, code) {
    let state = "none"
    if (code == 1) {
        state = ""
    }
    document.getElementById(id).style.display = state
}